import { User } from "./user";

export class Payments {
    paymentId!:number;
    totalAmt!:number;
    duration!:number;
    monthlyEMI!:number;
    customer!:User;
}
